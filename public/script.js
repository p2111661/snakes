var canvasWidht;
var canvaHeight;

var lineWidth;
var lineHeight;
var caseSize = 20;

var keys = [];
var SNAKE = [];
var food;
var WORLD = [];
var walls = [];
var score;
var context;
var interval;
var lastDirection = 0;

const UP = 37;
const LEFT = 38;
const RIGHT = 40;
const DOWN = 39;

var lvlLoaded = false;
var levelId = 1;
var start = false;
var startPlay = false;
var delay = 200;

const launch = document.getElementById('launch');

const go = document.getElementById('go');
go.addEventListener('click', startGame);

const select = document.getElementById('select');
select.addEventListener('change', function () {
    loadLevel(this.value);
});

async function loadLevel(id) {
    levelId = id;
    start = false;
    try {
        let response = await fetch('levels/'+id+'.json');
        if (response.ok) {
            let data = await response.json();
            delay = data.delay;
            lineWidth = data.dimensions[0];
            lineHeight = data.dimensions[1];
            SNAKE = data.snake;
            food = data.food;
            walls = data.walls;
            lastDirection = 0;
            keys = [];
            lvlLoaded = true;
        } else {
            throw new Error('Network response was not ok.');
        }
    } catch (error) {
        console.log(error);
    }
}

function setWorld() {
    WORLD = [];
    //0 = vide
    for (let i = 0; i < lineWidth; i++) {
        WORLD[i] = [];
        for (let j = 0; j < lineHeight; j++) {
            WORLD[i][j] = 0;
        }
    }
    //console.log(WORLD.length);
    //1 signifie que la case est un fruit
    for (let i = 0; i < food.length; i++) {
        WORLD[food[i][0]][food[i][1]] = 1;
    }
    //2 signifie que la case est un mur
    for (let i = 0; i < walls.length; i++) {
        WORLD[walls[i][0]][walls[i][1]] = 2;
    }
    //3 signifie que la case est un serpent
    for (let i = 0; i < SNAKE.length; i++) {
        //4 signifie que la case est la tête du serpent
        if (i == 0) {
            WORLD[SNAKE[i][0]][SNAKE[i][1]] = 4;
        } else {
            WORLD[SNAKE[i][0]][SNAKE[i][1]] = 3;
        }
    }
}

function newFruit() {
    let x = Math.floor(Math.random() * lineWidth);
    let y = Math.floor(Math.random() * lineHeight);
    if (WORLD[x][y] == 0) {
        WORLD[x][y] = 1;
    } else {
        newFruit();
    }
}

function incrementeScore() {
    score++;
    document.getElementById('score').textContent = score;
}

function resetScore() {
    score = 0;
    document.getElementById('score').textContent = 0;
}

function getLineGradient() {
    var line = context.createRadialGradient(
        canvasWidht,
        canvaHeight,
        Math.max(canvaHeight, canvasWidht),
        0,
        0,
        0
    );
    line.addColorStop(0, '#a1b6cc');
    line.addColorStop(1, '#8a8563');
    return line;
}

async function startGame() {
    if (!lvlLoaded) {
        await loadLevel('1');
        lvlLoaded = true;
    }
    //on crée le monde
    setWorld();
    score = 0;
    start = true;

    calculateMaxCaseSize();
    //taille
    canvasWidht = lineWidth * caseSize;
    canvaHeight = lineHeight * caseSize;

    //crée un canva avec selon les dimensions et supprimer le premier canva
    let ancienCanva = document.getElementById('game');
    let nouvCan = document.createElement('canvas');
    let parentDiv = document.getElementsByClassName('head')[0];
    nouvCan.setAttribute('id', 'game');
    nouvCan.setAttribute('width', canvasWidht);
    nouvCan.setAttribute('height', canvaHeight);
    nouvCan.style.display='block';
    parentDiv.insertBefore(nouvCan, ancienCanva);
    ancienCanva.remove();

    //le contexte de dessin du canva
    context = nouvCan.getContext('2d');
    draw();
}

//détection de l'appuie d'une touche dans la balise body, on appel la fontion updateKeyDown
document.body.addEventListener('keydown', updateKeyDown);
//détection du relachement d'une touche dans la balise body, on appel la fontion updateKeyUp
document.body.addEventListener('keyup', updateKeyUp);

//fonction qui permet de valider le fait qu'une des flèches est activée
function updateKeyDown(e) {
    //on limite la detection de touche aux quatre flèches du clavier
    if (
        (e.keyCode == UP) ||
        (e.keyCode == LEFT) ||
        (e.keyCode == DOWN) ||
        (e.keyCode == RIGHT)
    ) {
        if (e.keyCode == UP && lastDirection == DOWN) {
            return;
        }
        if (e.keyCode == LEFT && lastDirection == RIGHT) {
            return;
        }
        if (e.keyCode == DOWN && lastDirection == UP) {
            return;
        }
        if (e.keyCode == RIGHT && lastDirection == LEFT) {
            return;
        }
        //si c'est le premier mouvement de la partie, on lance la fonction step
        if (!startPlay) {
            startPlay = true;
            step();
        }
        if (start) {
            keys[e.keyCode] = true;
        }
    }
}

//fonction qui permet de valider le fait qu'une des flèches est désactivée
function updateKeyUp(e) {
    //on limite la detection de touche aux quatre flèches du clavier
    if (
        (e.keyCode == UP) ||
        (e.keyCode == LEFT) ||
        (e.keyCode == DOWN) ||
        (e.keyCode == RIGHT)
    ) {
        if (e.keyCode == UP && lastDirection == DOWN) {
            return;
        }
        if (e.keyCode == LEFT && lastDirection == RIGHT) {
            return;
        }
        if (e.keyCode == DOWN && lastDirection == UP) {
            return;
        }
        if (e.keyCode == RIGHT && lastDirection == LEFT) {
            return;
        }
        if (start) {
            lastDirection = e.keyCode;
            keys[e.keyCode] = false;
        }
    }
}

function update() {
    //on récupère la position de la tête du serpent
    let x = SNAKE[0][0];
    let y = SNAKE[0][1];
    //on déplace la tête du serpent en fonction de la touche appuyée
    if (keys[UP]) {
        y--;
    } else if (keys[LEFT]) {
        x--;
    } else if (keys[DOWN]) {
        y++;
    } else if (keys[RIGHT]) {
        x++;
    } else {
        if (lastDirection == UP) {
            y--;
        } else if (lastDirection == LEFT) {
            x--;
        } else if (lastDirection == DOWN) {
            y++;
        } else if (lastDirection == RIGHT) {
            x++;
        }
    }
    //on vérifie si la tête du serpent est sur un mur
    if (x > WORLD.length - 1 || y > WORLD[0].length - 1 || x < 0 || y < 0) {
        reset('Game Over : Vous avez heurté un mur.');
        resetScore();
        loadLevel(levelId);
        return;
    } else if (WORLD[x][y] == 1) {
        //on vérifie si la tête du serpent est sur un fruit
        incrementeScore();
        newFruit();
    } else if (WORLD[x][y] == 3) {
        reset('Game Over : Vous avec mangé votre corps.');
        resetScore();
        loadLevel(levelId);
        return;
    } else if (WORLD[x][y] == 2) {
        reset('Game Over : Vous avez heurté un mur.');
        resetScore();
        return;
    } else {
        //on supprime la queue du serpent
        WORLD[SNAKE[SNAKE.length - 1][0]][SNAKE[SNAKE.length - 1][1]] = 0;
        SNAKE.pop();
    }
    //on ajoute la tête du serpent
    WORLD[x][y] = 4;
    SNAKE.unshift([x, y]);
    //le reste du serpent est le corps
    for (let i = 1; i < SNAKE.length; i++) {
        WORLD[SNAKE[i][0]][SNAKE[i][1]] = 3;
    }
}

function step() {
    interval = setInterval(function () {
        if (start) {
            if (lastDirection != 0) {
                update();
            }
            draw();
        }
    }, delay);
}

function reset(message) {
    start = false;
    startPlay = false;
    clearInterval(interval);
    alert(message + '\n\nScore: ' + score+'\n\nR ou Entrée pour recommencer la partie !');
    resetScore();
    loadLevel(levelId);
}

function draw() {
    for (let i = 0; i < WORLD.length; i++) {
        for (let j = 0; j < WORLD[i].length; j++) {
            drawCase(i,j);
        }
    }
    buildSeparationGrid();
}

function buildSeparationGrid() {
    for (var i = 0; i <= canvasWidht; i += caseSize) {
        context.moveTo(0.5 + i, 0);
        context.lineTo(0.5 + i, canvaHeight);
    }
    for (i = 0; i <= canvaHeight; i += caseSize) {
        context.moveTo(0, 0.5 + i);
        context.lineTo(canvasWidht, 0.5 + i);
    }
    context.strokeStyle = 'black';
    context.stroke();
}

function calculateMaxCaseSize() {
    var deviceWidth = window.innerWidth > 0 ? window.innerWidth : screen.width;
    var deviceHeight =
        window.innerHeight > 0 ? window.innerHeight : screen.height;
    // On cherche la taille maximale de la case en largeur
    // Math.trunc(deviceWidth / lineWidth)
    deviceWidth = deviceWidth * 0.95;
    // On cherche la taille maximale de la case en hauteur
    // Math.trunc(deviceHeight / lineHeight)
    deviceHeight = deviceHeight * 0.85;

    // On prend la valeur minimale
    /*La fonction Math.trunc() retourne la troncature entière 
    d'un nombre en retirant sa partie décimale.*/
    caseSize = Math.min(
        Math.trunc(deviceWidth / lineWidth),
        Math.trunc(deviceHeight / lineHeight)
    );
}

function showTuto() {
    var showen = sessionStorage.getItem('tuto') ? true : false;
    sessionStorage.setItem('tuto', true);
    return showen;
}

window.onload = async function () {
    if (!showTuto()) {
        document.getElementById('Tutoriel').style.display = 'block';
    }
};

function drawCase(i,j){
    context.fillStyle =
                (i + j) % 2 === 0 ? '#9c6fd6' : getLineGradient();
            if (WORLD[i][j] == 1) {
                context.fillStyle = 'red';
            } else if (WORLD[i][j] == 2) {
                context.fillStyle = 'black';
            } else if (WORLD[i][j] == 3) {
                context.fillStyle = 'green';
            } else if (WORLD[i][j] == 4) {
                context.fillStyle = 'blue';
                
            }
            context.fillRect(
                j * caseSize,
                i * caseSize,
                caseSize - 1,
                caseSize - 1
            );
}

//listener des touches
document.onkeyup = function(e){

    if((e.key === "s" || e.key === "S" || e.key === "r" || e.key === "R")){
        go.click();
        go.focus();
    }

    if(e.key === "j" || e.key === "J"){
        launch.click();
    }

};